<?php
session_start();
require_once("functions.php");

//Checks if "addIngredient" form has been submitted, if true calls the add and update functions
if (isset($_REQUEST['addIngredient'])){
	$ingredientId=db_quote($_REQUEST['ingredient']);

	AddIngredient($_SESSION['pizzaId'],$ingredientId);
	UpdatePrice($_SESSION['pizzaId']);
}

//Checks if "removeIngredient" form has been submitted, if true calls the remove and update functions
else if (isset($_REQUEST['removeIngredient'])){
	$ingredientId=db_quote($_REQUEST['ingredient']);
	RemoveIngredient($_SESSION['pizzaId'],$ingredientId);
	UpdatePrice($_SESSION['pizzaId']);
}

//Calls function get all the pizzas
$rowsPizza=GetPizzas();
if($rowsPizza === false) {
    $error = db_error();
    echo $error;
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
	<title>Pizza Test</title>
	<meta charset="UTF8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="css/css.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

</head>
<body>

	<div class="container">
		<div class="page-header">
			<a href="index.php"><h1>Pizzas Menu</h1></a>
		</div>
	 
		<table class="table table-hover">
			<thead>
				<tr>
					<th><h4>Pizza</h4></th>
					<th><h4>Ingredients</h4></th>
					<th><h4>Price</h4></th>
					<th><h4>Actions</h4></th>
				</tr>
			</thead>
			<tbody>
			<?php foreach ($rowsPizza as $row){ ?>

				<tr class="PizzaRow" id="<?php echo $row['id'] ?>">
					<td> <?php echo $row['name'] ?></td>
					<td> <?php echo ShowPizzaIngredientsAsString($row['id']);?></td>
					<td> <?php echo ($row['price'] ? $row['price'] : 0) . "€"; ?></td>
					<td> <a href='addIngredient.php?id=<?php echo $row['id']; ?>&name=<?php echo $row['name']; ?>' class="btn btn-success btn-xs add">Add Ingredient</a>  <a href='removeIngredient.php?id=<?php echo $row['id']; ?>&name=<?php echo $row['name']; ?>' class="btn btn-danger btn-xs remove">Remove Ingredient</button></td>

				</tr>
			<?php } ?>
			</tbody>
		</table>
	</div>

</body>

</html>