<?php
session_start();
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
require_once("functions.php");


$pizza=db_quote($_GET['name']);
$pizzaId=db_quote($_GET['id']);

$_SESSION['pizzaId']=$pizzaId;

$ingredients=ShowPizzaIngredients($pizzaId);
?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Pizzes Test</title>
		<meta charset="UTF8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="css/css.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
		<script src="js/js.js"></script>

	</head>
	<body>

		<div class="container">
			<div class="page-header">
				<a href="index.php"><h1>Pizzas Menu</h1></a>
			</div>
			<div class="row">
				<div class="col-sm-6">
					<form action="index.php" method="post">
						<div class="form-group">
								<h2>Remove ingredient from pizza <?php echo $pizza ?> </h2>
								<select id="selectIngredient" name="ingredient" class="form-control">
									<?php
										foreach ($ingredients as $ingredient)
											echo "<option value='". $ingredient['id']."'>" . $ingredient['name'] . " (".$ingredient['price']." €)</option>";
									?>
								</select>
								<br/><br/>
								<h3 id="allRemoved"></h3>
								<button id="submit" type="submit" name="removeIngredient" class="btn btn-default">Remove Ingredient</button>
						</div>
					</form>
				</div>
				<div class="col-sm-6">
					<img src="img/pizza.jpg"/>
				</div>
			</div>
		</div>
	</body>
</html>