$(document).ready(function(){
	if(!document.getElementById('selectIngredient').length){
		document.getElementById("submit").disabled = true;
		$('#selectIngredient').hide();
		$('#allAdded').html('All the ingredients have already been added!');
		$('#allRemoved').html('All the ingredients have been removed!');
	}
});