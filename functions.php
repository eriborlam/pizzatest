<?php
require_once("conn.php");

//Shows all the pizzas on the database
function GetPizzas() {

	$rows = db_select ("SELECT * from `pizzas` ORDER BY price ASC,name ASC");

	return $rows;
}

//Returns all the ingredients from a pizza as array
function ShowPizzaIngredients($pizzaId) {
	
	$rows = db_select ("SELECT * from `ingredients` AS i JOIN `pizza_ingredients` AS ip ON (ip.ingredientId=i.id) WHERE pizzaId=" . $pizzaId . " ORDER BY i.price ASC, i.name ASC");

	if($rows === false) {
		$error = db_error();
		return $error;
	}
	
	return $rows;
}

//Inserts passed ingredientId to passed pizzaId
function AddIngredient($pizzaId,$ingredientId){

	// An insertion query. $result will be `true` if successful

	$result = db_query("INSERT INTO `pizza_ingredients` VALUES (" . $pizzaId . "," . $ingredientId . " )");

	return $result;
}

//Removes passed ingredientId from passed pizzaId
function RemoveIngredient($pizzaId,$ingredientId) {

	$result = db_query("DELETE FROM `pizza_ingredients` where pizzaId=" . $pizzaId . " and ingredientId=" . $ingredientId . "");

	return $result;
}

//Updates price from given pizzaId
function UpdatePrice($pizzaId) {

	$result = db_query("UPDATE `pizzas` SET price=((SELECT SUM(i.price) FROM ingredients AS i JOIN pizza_ingredients AS ip ON (i.id=ip.ingredientId) WHERE ip.pizzaId=" .$pizzaId . ") * 1.5) where id=" . $pizzaId . "");

	return $result;

}	

//Shows all the ingredients from a pizza as string
function ShowPizzaIngredientsAsString($pizzaId) {

	$ingredients="";
	
	$rows = ShowPizzaIngredients($pizzaId);
	
	 foreach ($rows as $row) { 
	 
	 $ingredients .=$row['name'].', '; 

	 }
	 
	 $ingredients=substr($ingredients, 0, -2);
	 
	return $ingredients;
}

//Shows all the ingredients that are not in the given pizzaId
function showIngredientsNotInPizza($pizzaId) {
	
	$rows= db_select("SELECT * FROM `ingredients` WHERE id NOT IN (SELECT id FROM `ingredients` AS i JOIN `pizza_ingredients` AS ip ON (i.id=ip.`ingredientId`) WHERE ip.`pizzaId`=" .$pizzaId . ") ORDER BY `price` ASC,`name` ASC;");
	
		if($rows === false) {
		$error = db_error();
		return $error;
	}
	
	return $rows;
}