/*
SQLyog Ultimate v11.11 (64 bit)
MySQL - 5.7.18-0ubuntu0.17.04.1 : Database - test
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`test` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `test`;

/*Table structure for table `ingredients` */

DROP TABLE IF EXISTS `ingredients`;

CREATE TABLE `ingredients` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `price` float unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `ingredients` */

insert  into `ingredients`(`id`,`name`,`price`) values (6,'Tomato',0.5),(7,'Sliced mushrooms',0.5),(8,'Feta cheese',1),(9,'Sausages',1),(10,'Sliced onion',0.5),(11,'Mozzarella cheese',0.5),(12,'Oregano',1),(13,'Bacon',1),(14,'Chicken',1),(15,'BBQ Sauce',0.5),(16,'Napolitana Sauce',0.5),(17,'Mince',1),(18,'Chilli Sauce',0.5),(19,'Green Pepper',1),(20,'Pepperoni ',1);

/*Table structure for table `pizza_ingredients` */

DROP TABLE IF EXISTS `pizza_ingredients`;

CREATE TABLE `pizza_ingredients` (
  `pizzaId` int(11) NOT NULL,
  `ingredientId` int(11) NOT NULL,
  PRIMARY KEY (`pizzaId`,`ingredientId`),
  KEY `ingrediente_id` (`ingredientId`),
  CONSTRAINT `pizza_ingredients_ibfk_1` FOREIGN KEY (`pizzaId`) REFERENCES `pizzas` (`id`) ON DELETE CASCADE,
  CONSTRAINT `pizza_ingredients_ibfk_2` FOREIGN KEY (`ingredientId`) REFERENCES `ingredients` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `pizza_ingredients` */

insert  into `pizza_ingredients`(`pizzaId`,`ingredientId`) values (1,6),(2,6),(3,6),(4,6),(5,6),(6,6),(7,6),(1,7),(2,7),(1,8),(3,8),(6,8),(7,8),(1,9),(2,9),(1,10),(1,11),(2,11),(1,12),(4,12),(2,13),(4,13),(7,14),(7,15),(4,16),(5,17),(5,18),(5,19),(6,20);

/*Table structure for table `pizzas` */

DROP TABLE IF EXISTS `pizzas`;

CREATE TABLE `pizzas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `price` float unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `pizzas` */

insert  into `pizzas`(`id`,`name`,`price`) values (1,'The Fun Pizza',7.5),(2,'The Super Mushroom Pizza',5.25),(3,'Margherita',2.25),(4,'Hawaiian',4.5),(5,'Mexicana',4.5),(6,'Pepperoni',3.75),(7,'BBQ Chicken',4.5);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
